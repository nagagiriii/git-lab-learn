package io.springboot.gitlab.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class GitLabLearnApplication {

	public static void main(String[] args) {
		System.out.println("Hello Gitlab");
		SpringApplication.run(GitLabLearnApplication.class, args);
	}
	
	public void test() {
		System.out.println("my test");
	}

}
